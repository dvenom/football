/**
 * @author: Tapra
 * @project: ARF Coach
 * 
 * @class:
 * 
 * @purpose:
 *
 * @attributes:
 */

package model.exception;

@SuppressWarnings("serial")
public class ARFCException  extends Exception {

   
   public ARFCException(){
      super("Default Exception");
   }
   // NOTE: it is advisable to use this constructor when creating new exceptions
   
   public ARFCException(String message) {
      super(message);
   }
}
