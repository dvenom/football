/**
 * @author: Tapra
 * @project: ARF Coach
 * 
 * @class:
 * 
 * @purpose:
 *
 * @attributes:
 */
 
package control;

public class NG {

   
   private static int lowEnd, highEnd;
   
   public NG(){}
   
   public NG(int i){}
   
   public static int generate(int low, int high){
      int result = 0;
      int range;
      if(high>low){
         highEnd = high;
         lowEnd = low;
      } else {
         highEnd = low;
         lowEnd = high;
      }
      range = highEnd - lowEnd;
      result = lowEnd + (int)(Math.random() * range);
      return result;
   }
   
   public int[] genMulti(int numRolls,int min,int max){
      int[] temp = new int[numRolls];
      int index = 0;
      for (int count = 0;count<numRolls;count++){
         temp[index] = generate(min, max);
         index++;
      }
      return temp;
   }
}
